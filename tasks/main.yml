---
- name: Install shibboleth
  apt:
    name: "{{item}}"
    state: latest
  loop:
    - shibboleth-sp2-common
    - shibboleth-sp2-utils
    - libapache2-mod-shib2
    - python-lxml

  when: not shibboleth_new_version

- name: Install shibboleth
  apt:
    name: "{{item}}"
    state: latest
  loop:
    - shibboleth-sp-common
    - shibboleth-sp-utils
    - libapache2-mod-shib
    - "{{\"python3-lxml\" if ansible_distribution_release == \"bullseye\" else \"python-lxml\"}}"
  when: shibboleth_new_version
    
- name: enable mod_shib2
  apache2_module: name=shib2 state=present
  ignore_errors: yes
  notify:
   - enable shib2 apache module
   - reload apache2
  when: not shibboleth_new_version

- name: enable mod_shib
  apache2_module: name=shib state=present
  ignore_errors: yes
  notify:
   - enable shib apache module
   - reload apache2
  when: shibboleth_new_version
   
   
- name: Check if keys exist
  shell: test -e /etc/shibboleth/sp-key.pem -a -e /etc/shibboleth/sp-cert.pem && echo yes || echo no
  register: shibboleth_keys_exist
- name: Generate keys for shibboleth
  shell: cd /etc/shibboleth && /usr/sbin/shib-keygen -f
  when: shibboleth_keys_exist.stdout == "no"
  notify: configured shibboleth

- name: Deploy shibboleth apache configuration on opac
  vars:
    require_shibboleth: "{{item.require_shibboleth is defined and (item.require_shibboleth == 'opac' or item.require_shibboleth == 'both')}}"
    use_headers: "{{item.shibboleth_use_headers is defined and item.shibboleth_use_headers or item.shibboleth_use_headers is not defined and shibboleth2_use_headers}}"
  template:
    src: koha-shibboleth.conf.in
    dest: /etc/apache2/vhost.d/{{item.name}}-opac/20-koha-shibboleth.conf
  notify:
    - configured shibboleth
    - restart apache2
  loop: "{{koha_libraries}}"
  when: item.enable_shibboleth is defined and (item.enable_shibboleth == "opac" or item.enable_shibboleth == "both")

- name: Deploy shibboleth apache configuration on intra
  vars:
    require_shibboleth: "{{item.require_shibboleth is defined and (item.require_shibboleth == \"intra\" or item.require_shibboleth == \"both\")}}"
    use_headers: "{{item.shibboleth_use_headers is defined and item.shibboleth_use_headers or item.shibboleth_use_headers is not defined and shibboleth2_use_headers}}"
  template:
    src: koha-shibboleth.conf.in
    dest: /etc/apache2/vhost.d/{{item.name}}-intra/20-koha-shibboleth.conf
  notify:
    - configured shibboleth
    - restart apache2
  loop: "{{koha_libraries}}"
  when: item.enable_shibboleth is defined and (item.enable_shibboleth == "intra" or item.enable_shibboleth == "both")

- name: Deploy shibboleth configuration
  template:
    src: shibboleth2.xml.in
    dest: /etc/shibboleth/shibboleth2.xml
  notify:
   - configured shibboleth
   - restart shibd

- name: Enable shibboleth
  xml:
    path: "/etc/koha/sites/{{item.name}}/koha-conf.xml"
    xpath: /yazgfs/config/useshibboleth
    value: "1"
    state: present
  loop: "{{ koha_libraries }}"
  when: item.enable_shibboleth is defined
  notify:
   - configured shibboleth
   - restart plack

- name: Add shibboleth configuration element
  xml:
    path: "/etc/koha/sites/{{item.name}}/koha-conf.xml"
    xpath: /yazgfs/config/shibboleth
    state: present
  loop: "{{ koha_libraries }}"
  when: item.enable_shibboleth is defined
  notify:
   - configured shibboleth
   - restart plack

- name: Set shibboleth configuration
  xml:
    path: "/etc/koha/sites/{{item.name}}/koha-conf.xml"
    xpath: /yazgfs/config/shibboleth
    input_type: xml
    state: present
    set_children:
      - "<matchpoint>{{item.shibboleth_matchpoint | default(\"userid\") | escape}}</matchpoint>"
      - |-
        <mapping>
          <userid is="{{item.shibboleth_userid_mapping | default("eduPersonID")}}"></userid> <!-- koha borrower field to shibboleth attribute mapping -->
          {% for mapping in (item.shibboleth_userid_mappings | default([])) %}
          <{{mapping.target}} is="{{mapping.source}}"></{{mapping.target}}>
          {% endfor %}
        </mapping>
  loop: "{{ koha_libraries }}"
  when: item.enable_shibboleth is defined
  notify:
   - configured shibboleth
   - restart plack

- name: Add attribute mappings
  template:
    src: attribute-map.xml.in
    dest: "/etc/shibboleth/attribute-map.xml"
   
- meta: flush_handlers
